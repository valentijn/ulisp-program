;; Ports with LEDs plugged in

(defvar pins '(3 5 6 9 10 11))

;; LED states
(defvar high 255)
(defvar half 128)
(defvar low 0)

;; Delay
(defvar timeout 800)

(defun blinkLED (port)
  (analogwrite port high)
  (delay timeout)
  (analogwrite port low))

(defun onLED (port)
  (analogwrite port high)
  (delay timeout))

(defun offLED (port)
  (analogwrite port low)
  (delay timeout))

(defun randomLED (ports)
  (blinkLED (nth (random (length ports)) ports)))

(defun lineLED (true false ports)
  (if (null ports)
      ;; Run function when the list empty
      (false)

    ;; Run function when list is not empty
    (progn (true (car ports))
           (lineLED true false (cdr ports)))))

(defun runLED (fun)
  (loop
   (fun pins)))

(lineLED onLED
         (lambda () (dolist (port (reverse pins)) (offLED port)))
         pins)
